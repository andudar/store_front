import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  CreateComponentList,
  AddProductComponent,
  PaginationComponent,
  EmptyPopupComponent,
  DatePickerComponent,
  TimeSelectorComponent,
  CustomSelectComponent} from './components';

import { ClickOutsideDirective } from './directives';
import { UpdateProfileComponent } from './components/update-profile/update-profile.component';

@NgModule({
  declarations: [
    CreateComponentList,
    AddProductComponent,
    EmptyPopupComponent,
    PaginationComponent,
    DatePickerComponent,
    TimeSelectorComponent,
    CustomSelectComponent,

    ClickOutsideDirective,
    UpdateProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [PaginationComponent],
  providers: []
})
export class SharedModule { }
