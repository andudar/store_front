import {Component, OnInit, Output, EventEmitter, ViewChild, Input} from '@angular/core';
import {CurrentDate, CalendarItem } from './CurrentDate';
import { DateService } from './../../services';
import { DAYS_SHORT } from './../../constants';
import {Element} from "@angular/compiler/src/ml_parser/ast";

interface Time {
  hours: string,
  minutes: string
}

@Component({
  selector: 'date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})

export class DatePickerComponent implements OnInit {

  @Input() allowSelectPrevDate: boolean;
  @Input() movingByYears: boolean;
  @Input() hide: boolean = false;
  @Input() purchaseDate: any;

  @Output() select: EventEmitter<Date> = new EventEmitter();
  @Output() show: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('input') input;

  public isDatePicker: boolean = false;
  public dayIsSelected: boolean = false;
  public selectedDate: Date = new Date();
  public day;
  public date: CurrentDate;
  public time: Time;
  public DAYS_SHORT: Array<string>;

  public calendar: any;
  // public calendar: Array<[CalendarItem]>;
  private todayDate: Date;

  constructor() {
  }

  ngOnInit() {
    this.DAYS_SHORT = DAYS_SHORT;
    this.date = new CurrentDate();
    this.todayDate = new Date(this.date.year, this.date.month, this.date.date);

    this.calendar = DateService.createCalendar(this.date.year, this.date.month);
  }

  private navClasses = {
    forward: '_go-forward',
    back: '_go-back',
  };

  private itemClass: string = 'month-day-available';
  private itemSelectedClass: string = 'selectedItem';
  private navClass: string = 'date-nav-action';

  public moveByMonth(event: MouseEvent) {
    let
      target = <any>event.target,
      monthIdx;

    if (!target.classList.contains(this.navClass)) {
      return;
    }

    if (target.classList.contains(this.navClasses.forward) || target.parentNode.classList.contains(this.navClasses.forward)) {
      monthIdx = this.date.month + 1;
    }

    if (target.classList.contains(this.navClasses.back) || target.parentNode.classList.contains(this.navClasses.back)) {
      monthIdx = this.date.month - 1;
    }

    this.date.update(this.date.year, monthIdx);
    this.calendar = DateService.createCalendar(this.date.year, this.date.month);
  }

  public moveByYear(event: MouseEvent) {
    let
      target = <any>event.target,
      year;

    if (!target.classList.contains(this.navClass)) {
      return;
    }

    if (target.classList.contains(this.navClasses.forward) || target.parentNode.classList.contains(this.navClasses.forward)) {
      year = this.date.year + 1;
    }

    if (target.classList.contains(this.navClasses.back) || target.parentNode.classList.contains(this.navClasses.back)) {
      year = this.date.year - 1;
    }

    this.date.update(year, this.date.month);
    this.calendar = DateService.createCalendar(this.date.year, this.date.month);
  }

  public selectDate(event) {
    if (!this.dateIsAvailableForSelect(event.day)) {
      return;
    }

    this.selectedDate = event.day.date;
    this.dayIsSelected = true;
  }

  public isSelectedDay(day: CalendarItem) {
    return DateService.compareShortDates(this.selectedDate, day.date);
  }

  public dateIsAvailableForSelect(day: CalendarItem) {
    /* If we can select prev date we just interested that it is current month,
     otherwise, check if this day have already passed */
    return this.allowSelectPrevDate ? day.currentMonth : day.currentMonth && +day.date >= +this.todayDate;
  }

  public applyTime(event: Time) {
    this.selectedDate.setHours(+event.hours);
    this.selectedDate.setMinutes(+event.minutes);

    this.input.nativeElement.value = DateService.reformatDate(this.selectedDate);
    this.select.next(this.selectedDate);
  }

  public onClose() {
    this.dayIsSelected = false;
    this.isDatePicker = false;
  }

  ngOnChanges(changes) {
    if (changes.hide) {
      this.isDatePicker = changes.hide.currentValue;
    }

    /* Set date if we are in update mode and we already have a value */
    if (changes.purchaseDate && changes.purchaseDate.currentValue) {

      this.date.updateFromString(changes.purchaseDate.currentValue);

      this.calendar = DateService.createCalendar(this.date.year, this.date.month);
      this.selectedDate = new Date(changes.purchaseDate.currentValue);
      this.time = {
        hours: this.selectedDate.getHours() + '',
        minutes: this.selectedDate.getMinutes() + '',
      };

      this.input.nativeElement.value = DateService.reformatDate(this.selectedDate);
    }
  }

//TODO remove logic to service?
}
