/**
 * Created by andrey on 19.01.17.
 */

export interface CalendarItem {
  currentMonth: boolean,
  date: Date,
  value: number
}

export class CurrentDate {
  private _date: Date;

  constructor() {
    this._date = new Date();
  }

  public get year() {
    return this._date.getFullYear();
  }

  public get month() {
    return this._date.getMonth();
  }

  public get date() {
    return this._date.getDate();
  }

  public get msValue() {
    return +this._date;
  }

  public get msValueWithoutHours() {
    return +new Date(this.year, this.month, this.date);
  }

  public update(year, month) {
    this._date = new Date(year, month);
  }

  public updateFromString(str: string) {
    this._date = new Date(str);
  }
}
