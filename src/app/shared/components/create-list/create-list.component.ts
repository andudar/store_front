import {Component, OnInit, ViewChild, Renderer } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from "@angular/router";
import {ProductsListService} from "../../../modules/main/services";
import {FormsService} from "../../../modules/auth/services/forms.service";

@Component({
  selector: 'create-list',
  templateUrl: 'create-list.component.html',
  styleUrls: ['create-list.component.scss']
})

export class CreateComponentList implements OnInit {
  @ViewChild('input') listNameEl;
  @ViewChild('createListForm') form;

  private submitted: boolean = false;
  private isCreated: boolean = false;
  private isError: boolean = false;
  private listId: number;

  public purchaseDate;

  private list: FormGroup;

  private isUpdateMode: boolean = false;
  public errMsgs: any = {
    name: [],
    //purchase_date: []
  };
  public isDatePicker: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private productsListService: ProductsListService,
              private formsService: FormsService,
              private renderer: Renderer) {
  }

  ngOnInit() {
    this.route.params.forEach(el => {
      if (el['listId']) {

        this.listId = el['listId'];
        this.isUpdateMode = true;
      }
    });

    this.list = this.formBuilder.group({
      'name': ['', [Validators.required]],
      //'purchase_date': ['', [Validators.required]]
    });

    this.list.valueChanges
      .subscribe(values => {
        FormsService.checkFormValidity('list', this.errMsgs, this.list.controls);
      });

    if (this.isUpdateMode) {
      this.productsListService.getList(this.listId).subscribe(list => {
        this.list.controls['name'].patchValue(list.name);
        this.purchaseDate = list.purchase_date;
        //this.list.controls['purchase_date'].patchValue(list.execution);
      })
    }

  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.listNameEl.nativeElement, 'focus');
  }

  private createList(name) {
    this.submitted = true;

    this.productsListService.createList(name, this.purchaseDate)
      .subscribe(res => {
        this.isCreated = true;
      },
      err => {
        console.error(err);
        this.isError = true;
      });
    //this.router.navigate(['/main/lists']);
    this.listNameEl.nativeElement.value = '';
  }


  public updateList(list) {
    this.submitted = true;

    if (list.invalid) {
      return;
    }

    this.productsListService.updateList(this.listId, {name: list.value.name, purchase_date: this.purchaseDate}).subscribe(
      res => {
        this.isCreated = true;
      },
      err => {
        console.error(err);
        this.isError = true;
      });
    //this.router.navigate(['/main/lists']);
    this.listNameEl.nativeElement.value = '';
  }

  private clearErrors($event: KeyboardEvent) {
    // prevent erasing error messages after submitting by 'enter' key
    if ($event.keyCode == 13) {
      return;
    }
    this.isError = false;
    this.isCreated = false;
    this.submitted = false;
  }

  // public chooseDay() {
  //   this.isDatePicker = true;
  // }

  public setSelectedDate(date) {
    console.log(date);
    console.log(typeof date);
    this.purchaseDate = date;
  }

  public hideDatePicker() {
    this.isDatePicker = false;
  }

  private close($event: MouseEvent) {
    let target = <any>event.target;
    if (target.classList.contains('create-list__overflow') || target.classList.contains('create-list__actions_close')) {
      return this.router.navigate([{outlets: {popup: null}}])
    }
  }
}
