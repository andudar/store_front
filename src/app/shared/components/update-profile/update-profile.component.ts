import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { User } from "../../../models/User";
import { UserService } from "../../../modules/main/services/user.service";

@Component({
  selector: 'update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {

  public user: User;
  public isError: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['user'];
    console.log(this.user)
  }

  public onUpdateUser({name, email}: {name: string, email: string}) {
    const
      nameReg = /\w{3,}/,
      emailReg = /(\w+@(?=\b))|\w{0}/;
    if (!nameReg.test(name) || !emailReg.test(email)) {
      this.isError = true;
    }

    console.log(name, email);
    this.userService.updateProfile(name, email);
  }

  onChangeError() {
    if (this.isError) {}
    this.isError = false;
  }

  public close() {
    return this.router.navigate([{outlets: {popup: null}}])
  }

}
