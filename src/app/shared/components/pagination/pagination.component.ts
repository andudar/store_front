import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ProductList} from "../../../models/ProductList";
import {Product} from "../../../models/Product";
import { UtilityService } from './../../services';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})

export class PaginationComponent implements OnInit {

  @Input() itemsOnPage: number;
  @Input() items: Product[] = [];

  @Output() itemsToShow = new EventEmitter();

  public pages: Array<number>;

  /* We will decrease current page number to be appropriate index of array */
  private currentPage: number = 1;
  /* Current index to start show */
  private indexToStart = 0;
  private totalItemsAmount: number = 0;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    console.log(changes);
    if (changes.items && changes.items.currentValue.length) {
      this.totalItemsAmount = changes.items.currentValue.length;

      this.calcButtonsAmount(this.totalItemsAmount);
      this.moveByPages(this.currentPage);
    }
  }

  private calcButtonsAmount(itemsAmount) {
    let
      buttonsAmount = Math.ceil(itemsAmount / this.itemsOnPage);

    this.pages = UtilityService.arrayOfNums(buttonsAmount);
  }

  private moveByPages(pageNumber) {
    if (pageNumber < 1) {
      return;
    }

    this.currentPage = pageNumber;

    this.indexToStart = (pageNumber - 1) * this.itemsOnPage;

    let
      list = this.items.slice(this.indexToStart, this.indexToStart + this.itemsOnPage);

    /* Move to previous page if we've deleted the last item on this page */
    if (this.pages.indexOf(pageNumber) == -1) {
      console.log('it was the last page')
      /* Decrease index to previous page */
      this.currentPage--;
      this.indexToStart = (this.currentPage - 1) * this.itemsOnPage;

      list = this.items.slice(this.indexToStart, this.indexToStart + this.itemsOnPage);
    }

    this.itemsToShow.next(list);


    // setTimeout(() => {
    //   this.itemsToShow.next(list);
    // }, 0)
  }


}
