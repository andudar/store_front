import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';

export interface Items {
  label: string,
  value: Array<any>
}

@Component({
  selector: 'custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss']
})
export class CustomSelectComponent implements OnInit {

  @Input() items: Items;
  @Input() selected: any;
  @Output() select: EventEmitter<any> = new EventEmitter();

  public selectedItem: number = null;
  public showSelectList: boolean = false;
  public scroll: number = 0;

  private scrollTarget;
  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
  }

  public openSelect() {
    this.showSelectList = true;
    /* Return to the last scrolled position */
    this.scrollTo(this.scroll)
  }

  public selectItem(item) {
    /* Remember scroll position in the list */
    this.scroll = this.scrollTarget.scrollTop;
    /* Remember selectedItem item for applying special class to it */
    this.selectedItem = item;
    this.select.next(item);
    this.showSelectList = !this.showSelectList;
  }

  public scrollTo(scroll: number) {
    this.scrollTarget = this.elementRef.nativeElement.querySelector('.select__list');
    this.scrollTarget.scrollTop = scroll;
  }

  public onClose() {
    this.showSelectList = false;
  }

  ngOnChanges(changes) {
    if (changes.selected && changes.selected.currentValue) {
      this.selectedItem = changes.selected.currentValue;
    }
  }

}
