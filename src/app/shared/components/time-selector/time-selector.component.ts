import {Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UtilityService, DateService } from './../../services';
import { Items } from './../custom-select/custom-select.component';

interface Time {
  hours: string,
  minutes: string
}

@Component({
  selector: 'time-selector',
  templateUrl: './time-selector.component.html',
  styleUrls: ['./time-selector.component.scss']
})


export class TimeSelectorComponent implements OnInit {

  @Input() inputTime: Time;

  @Output() select: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  public innerTime: Time = {
    hours: '',
    minutes: ''
  };

  public hours: Items = {
    label: 'h',
    value: (() => {
      let
        arr = DateService.addAheadZeroes(UtilityService.arrayOfNums(24, 1));
      arr[arr.length - 1] = '00';
      return arr;
    })()
  };

  public minutes: Items = {
    label: 'min',
    value: DateService.addAheadZeroes(UtilityService.arrayOfNums(60, 0))
  };


  constructor() { }

  ngOnInit() {
  }

  public onSelectHours(event: string) {
    this.innerTime.hours = event
  }

  public onSelectMinutes(event: string) {
    this.innerTime.minutes = event
  }

  public selectTime() {
    this.select.next(this.innerTime);
    this.close.next();
  }

  public cancelSelection() {
    this.close.next();
  }

  ngOnChanges(changes: any) {
    if (changes.inputTime && changes.inputTime.currentValue) {
      console.log(changes.inputTime.currentValue);
      this.innerTime.hours = DateService.addAheadZeroToItem(changes.inputTime.currentValue.hours);
      this.innerTime.minutes = DateService.addAheadZeroToItem(changes.inputTime.currentValue.minutes);
    }
  }

}
