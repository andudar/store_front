import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'empty-popup',
  templateUrl: './empty-popup.component.html',
  styleUrls: ['./empty-popup.component.scss']
})
export class EmptyPopupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
