import {Component, OnInit, Input, ViewChild, Renderer} from '@angular/core';
import {Product, ProductList} from "../../../models";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, FormControl, Validators} from "@angular/forms";
import {ProductsService} from "../../../modules/main/services";
import {FormsService} from "../../../modules/auth/services/forms.service";
import {Subject} from "rxjs";

@Component({
  selector: 'add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {


  @ViewChild('name') nameEl;
  @ViewChild('amount') amountEl;

  private listId;
  private name;
  private amount;
  private productId;
  private submitted: boolean = false;
  private isCreated: boolean = false;
  private isUpdated: boolean = false;
  private isError: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private productService: ProductsService,
              private renderer: Renderer) {

    route.params.forEach(params => {
      this.listId = params['listId'];
      if (params['product']) {
        this.isUpdateMode = true;
        this.productId = params['product'];
      }
    })

  }

  public product: FormGroup;

  private formValues;
  public errMsgs: any = {
    name: [],
    amount: []
  };
  private isUpdateMode: boolean = false;

  ngOnInit() {
    this.product = this.formBuilder.group({
      'name': ['', [Validators.required, Validators.minLength(2)]],
      'amount': ['', [Validators.maxLength(10)]]
    });

    this.product.valueChanges
      .subscribe(values => {
        FormsService.checkFormValidity('product', this.errMsgs, this.product.controls);
      });

    if (this.isUpdateMode) {
      this.productService.getProduct(this.productId).subscribe(product => {
        this.product.controls['name'].patchValue(product.name);
        this.product.controls['amount'].patchValue(product.amount);
      })
    }

  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.nameEl.nativeElement, 'focus');
  }

  private createProduct({value}: {value: Product}) {

    FormsService.checkFormValidity('product', this.errMsgs, this.product.controls);

    if (this.product.invalid) {
      this.isError = true;
      return;
    }

    this.productService.addProduct(this.listId, value)
      .subscribe(res => {
          this.submitted = true;
          this.isCreated = true;

          this.product.controls['name'].patchValue('');
          this.product.controls['amount'].patchValue('');
        },
        err => {
          console.error(err);
          this.isError = true;
        });


  }

  private updateProduct(product: Product) {
    FormsService.checkFormValidity('product', this.errMsgs, this.product.controls);

    if (this.product.invalid) {
      this.isError = true;
      return;
    }

    this.productService.updateProduct(this.productId, this.product.value).subscribe(
      res => {
        this.submitted = true;
        this.isUpdated = true;

        this.product.controls['name'].patchValue('');
        this.product.controls['amount'].patchValue('');
      },
      err => {
        console.error(err);
        this.isError = true;
      });

  }

  private clearErrors($event: KeyboardEvent) {
    // prevent erasing error messages after submitting by 'enter' key
    if ($event.keyCode == 13) {
      return;
    }
    //TODO improve this piece
    this.submitted = false;
    this.isUpdated = false;
    this.isCreated = false;
    this.isError = false;
  }

  private close($event: MouseEvent) {
    let target = <HTMLElement>event.target;
    if (target.classList.contains('create-list__overflow') || target.classList.contains('create-list__actions_close')) {
      return this.router.navigate([{outlets: {popup: null}}])
    }
  }


}
