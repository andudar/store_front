/**
 * Created by andrey on 31.01.17.
 */
export const DAYS = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
];

/* Create array of days names by some rules */
export const DAYS_SHORT = (() => DAYS.map(el => el.slice(0, 3)))();
