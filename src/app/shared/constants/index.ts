/**
 * Created by andrey on 31.01.17.
 */
export { DAYS, DAYS_SHORT } from './DAYS';
export { MONTHS, MONTHS_SHORT } from './MONTHS';
