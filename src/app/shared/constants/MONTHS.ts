/**
 * Created by andrey on 24.01.17.
 */
export const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

/* Create array of months names by some rules */
export const MONTHS_SHORT = (() => {
  return MONTHS.map((el, idx) => {
    if (idx == 5 || idx == 6 || idx == 8) {
      return el.slice(0, 4);
    } else {
      return el.slice(0, 3);
    }
  });
})();
