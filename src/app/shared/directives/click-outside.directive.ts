import { Directive, ElementRef, HostListener, EventEmitter, Output } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})

export class ClickOutsideDirective {

  constructor(private elemRef: ElementRef) {
  }

  @Output() public clickOutside = new EventEmitter();

  @HostListener('document:click', ['$event', '$event.target'])
  private onClick(event, elemTarget) {
    if (!this.elemRef.nativeElement.contains(elemTarget)) {
      this.clickOutside.next();
      event.stopPropagation();
    }
  }
}
