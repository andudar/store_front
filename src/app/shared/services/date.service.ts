import {Injectable} from '@angular/core';
import {MONTHS, DAYS} from './../constants';
import {UtilityService} from './utility.service';

@Injectable()
export class DateService {

  constructor() {
  }

  static reformatDate(date: Date) {
    let
      year = date.getFullYear(),
      month = MONTHS[date.getMonth()],
      day = date.getDate(),
      hours = date.getHours() + '',
      minutes = date.getMinutes() + '';

    return `${month} ${day}, ${year} ${DateService.addAheadZeroToItem(hours)}:${DateService.addAheadZeroToItem(minutes)}`;
  }

  static addAheadZeroes(arr: Array<number>): any {
    return arr.map(DateService.addAheadZeroToItem)
  }

  static addAheadZeroToItem(item: any): string {
    let
      elStr: string = item + '';
    return elStr.length < 2 ? '0' + elStr : elStr;
  }

  /* Get an index from European set of days */
  static getStartDayIdx(year, monthIdx) {
    let
      d = new Date(year, monthIdx, 1),
      day = d.getDay();

    return day == 0 ? DAYS.length - 1 : day - 1;
  }

  static getLastDayIdx(year, monthIdx) {
    let
      monthLength = DateService.getMonthLength(year, monthIdx),
      d = new Date(year, monthIdx, monthLength),
      day = d.getDay();

    return day == 0 ? DAYS.length - 1 : day - 1;
  }

  static getMonthLength(year, monthIdx) {
    let
      outbalancingDate = 32,
      d = new Date(year, monthIdx, outbalancingDate);

    return outbalancingDate - d.getDate();
  }

  static createMonth(year, monthIdx) {
    let
      d = new Date(year, monthIdx),
      monthLength;

    monthLength = DateService.getMonthLength(d.getFullYear(), d.getMonth());
    return UtilityService.arrayOfNums(monthLength);
  }

  static createCalendar(year, monthIdx) {
    let
      currentMonth = DateService.createMonth(year, monthIdx),
      previousMonth,
      startDayIdx = DateService.getStartDayIdx(year, monthIdx),
      lastDayIdx = DateService.getLastDayIdx(year, monthIdx),
      nextMonth = UtilityService.arrayOfNums(7 - (lastDayIdx + 1)),
      nextMonthAdditionalPart = UtilityService.arrayOfNums(7 - (lastDayIdx + 1) + 7, 7 - (lastDayIdx + 1) + 1),
      resultStringWithDates: Array<string> = [],
      resultSet = [];

    previousMonth = DateService.createMonth(year, monthIdx - 1);
    //nextMonth = DateService.createMonth(year, monthIdx + 1); //TODO add interface


    /* Get set of dates from previous month */
    if (startDayIdx == 0) {
      previousMonth = previousMonth.slice(-7);
    } else {
      previousMonth = previousMonth.slice(-startDayIdx);
    }

    /* Create previous month */
    previousMonth = previousMonth.map(el => {
      return {value: el, currentMonth: false, date: new Date(year, monthIdx - 1, el)}
    });

    /* Create current month */
    currentMonth = currentMonth.map(el => {
      return {value: el, currentMonth: true, date: new Date(year, monthIdx, el)}
    });

    /* Create next month */
    nextMonth = nextMonth.map(el => {
      return {value: el, currentMonth: false, date: new Date(year, monthIdx + 1, el)}
    });

    /* Create additional part of next month */
    nextMonthAdditionalPart = nextMonthAdditionalPart.map(el => {
      return {value: el, currentMonth: false, date: new Date(year, monthIdx + 1, el)}
    });

    resultStringWithDates = [...previousMonth, ...currentMonth, ...nextMonth];
    //console.log(resultStringWithDates);

    let weeksAmount = Math.ceil(resultStringWithDates.length / 7);

    /* Create calendar always with 6 rows, if there are only five - add 6th additional row */
    if (weeksAmount < 6) {
      resultStringWithDates = [...resultStringWithDates, ...nextMonthAdditionalPart];
      weeksAmount += 1;
    }

    for (; weeksAmount--; ) {
      resultSet.push(resultStringWithDates.splice(0, 7))
    }

    // console.log('resultSet ', resultSet);
    return resultSet;
    // console.log('previousMonth ', previousMonth);
  }

  static createShortDate(date: Date) {
    let d = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    return {
      toNumber() {
        return +d;
      }
    }
  }

  static compareShortDates(date: Date, dateToCompare: Date) {
    return DateService.createShortDate(date).toNumber() == DateService.createShortDate(dateToCompare).toNumber();
  }

}
