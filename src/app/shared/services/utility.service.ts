import { Injectable } from '@angular/core';

@Injectable()
export class UtilityService {

  constructor() { }

  static arrayOfNums(amount, sliceFrom: number = 1) {
    return Array.apply(null, {length: amount + 1})
      //.map(Number.call, Number)
      .map((el, idx) => Number.call(null, idx))
      .slice(sliceFrom);
  }
}
