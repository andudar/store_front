/**
 * Created by andrey on 10.11.16.
 */
export interface User {
  name: string,
  email: string,
  token: string
}
