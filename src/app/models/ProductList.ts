/**
 * Created by andrey on 12.12.16.
 */
import {Product} from "./Product";

export interface ProductList {
  _id: number,
  _owner: string,
  name: string,
  _products: Array<Product>,
  createdAt: Date,
  updatedAt: Date,
}
