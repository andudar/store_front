/**
 * Created by andrey on 12.12.16.
 */
export interface Product {
  name: string,
  amount?: number,
  _id?: number
}
