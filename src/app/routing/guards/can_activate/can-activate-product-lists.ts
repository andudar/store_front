import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import { AuthService } from './../../../modules/auth/services';

@Injectable()
export class CanActivateProductLists implements CanActivate {
  constructor(private authService: AuthService) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log(route.routeConfig);
    if (route.routeConfig.path === "lists") {
      return this.authService.isLogged;
    }
  }
}


