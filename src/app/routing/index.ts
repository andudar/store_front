/**
 * Created by andrey on 07.11.16.
 */
import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent, SignUpComponent, IndexComponent} from './../modules/auth/components';
import {
  MainComponent,
  NavbarComponent,
  ProductListAllComponent,
  ProductListComponent,
  ProfileComponent,
  StaticsComponent
} from "../modules/main/components";

import { CreateComponentList, AddProductComponent, EmptyPopupComponent, UpdateProfileComponent } from './../shared/components';

import { ProductsResolver, UserResolver } from './resolvers';
import { CanActivateProductLists } from './guards';

const routes: Routes = [
  {
    path: '', component: IndexComponent
  },
  {
    path: 'signin', component: SignInComponent
  },
  {
    path: 'signup', component: SignUpComponent
  },
  {
    path: 'main',
    children: [
      {
        path: '',
        component: MainComponent,
        children: [
          {
            path: 'lists',
            component: ProductListAllComponent,
            // resolve: {
            //   productLists: ProductsResolver
            // },
            data: {
              hello: 'hello from resolver'
            },
            canActivate: [CanActivateProductLists],
          },
          {
            path: 'list/:id',
            component: ProductListComponent,
          },
          {
            path: 'profile',
            component: ProfileComponent
          },
          {
            path: 'statics',
            component: StaticsComponent
          },
          {
            path: '**',
            redirectTo: 'lists'
          }
        ]
      }
    ]
  },
  {
    path: 'create-list',
    component: CreateComponentList,
    outlet: 'popup'
  },
  {
    path: ':listId/add-product',
    component: AddProductComponent,
    outlet: 'popup'
  },
  {
    path: 'update-profile',
    component: UpdateProfileComponent,
    outlet: 'popup',
    resolve: {
      user: UserResolver
    }
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { enableTracing: true });
