/**
 * Created by andrey on 05.12.16.
 */
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {User} from "../../models/User";
import { ProductsListService } from '../../modules/main/services';
import {Observable, Observer} from "rxjs";


@Injectable()
export class ProductsResolver implements Resolve<any> {
  constructor(private productService: ProductsListService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  Observable<any> {
    return this.productService.product_lists;
  }
}
