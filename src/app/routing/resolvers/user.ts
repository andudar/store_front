import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { User } from "../../models/User";
import { AuthService } from "../../modules/auth/services/auth.service"

@Injectable()
export class UserResolver implements Resolve<User> {
  constructor(private router: Router, private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): User {
    let user: User = this.authService.getUser();

    if (user) {
      return user;
    }
  }
}
