import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {ProductsListService} from "../../modules/main/services";
import {Observable} from "rxjs";
/**
 * Created by andrey on 14.12.16.
 */
export class ListResolver implements Resolve<any> {
  constructor(private productService: ProductsListService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  Observable<any> {
    return this.productService.getProductsList(route.params['id'])
  }
}

// this.productService.getList(listId).subscribe(list => {
//   console.log(list)
//   this.router.navigate([`main/list/${listId}`])
// })
