import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AuthModule, MainModule, ApiModule } from './modules';
import { SharedModule } from './shared';
import { AppComponent } from './app.component';

import { routing } from "./routing";
import { CanActivateProductLists } from "./routing/guards";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,

    ApiModule,
    AuthModule,
    MainModule,

    SharedModule,

    routing
  ],
  providers: [CanActivateProductLists],
  bootstrap: [AppComponent]
})
export class AppModule { }
