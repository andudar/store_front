/**
 * Created by andrey on 07.11.16.
 */
export { AuthModule } from './auth/auth.module';
export { MainModule } from './main/main.module';
export { ApiModule } from './api/api.module'
