/**
 * Created by andrey on 07.11.16.
 */
export { FormsService } from './forms.service';
export { AuthService } from './auth.service';
export { LocalStorageService } from './local-storage.service';
