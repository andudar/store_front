import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { LocalStorageService } from './local-storage.service';
import { ApiService } from './../../api/services';

const TOKEN_KEY = 'auth-token';

@Injectable()
export class AuthService {
  public isLogged = false;

  constructor(private apiService: ApiService, private router: Router) {
    this.isLogged = !!LocalStorageService.get(TOKEN_KEY);
  }

  signin(credentials) {
    return this.apiService.call({
      method: 'post',
      url: '/signin',
      body: credentials,
    });
  }

  signup(credentials) {
    return this.apiService.call({
      method: 'post',
      url: '/signup',
      body: credentials,
    });
  }

  saveUser(token: string): void {
    LocalStorageService.set(TOKEN_KEY, token);
    this.isLogged = true;
  }

  private isLoggedIn() {
    let
      token = LocalStorageService.get(TOKEN_KEY),
      payload;

    if (token) {
      payload = atob(token.split('.')[1]);
      payload = JSON.parse(payload);

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };

  public getUser() {
    let
      token = this.token;

    if (this.isLoggedIn()) {
      let
        payload;

      payload = atob(token.split('.')[1]);
      payload = JSON.parse(payload);

      return {
        email: payload.email,
        name: payload.name,
        token: token
      };
    }
  };

  public get token() {
    return LocalStorageService.get(TOKEN_KEY);
  }


  public logout() {
    LocalStorageService.remove(TOKEN_KEY);
    this.isLogged = false;
    this.router.navigate(['/']);

    this.apiService.call({
      method: 'post',
      url: '/logout',
      body: {},
    }).subscribe();
  }

}
