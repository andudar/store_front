import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }

  static get(key: string): string {
    return localStorage.getItem(key);
  }

  static set(key, value): void {
    localStorage.setItem(key, value);
  }

  static remove(key): void {
    localStorage.removeItem(key);
  }

}
