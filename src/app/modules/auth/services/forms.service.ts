import { Injectable } from '@angular/core';
import {AbstractControl} from "@angular/forms";

export interface SignInError {
  email: {
    required: string,
    minLength: string,
    pattern: string
  },
  password: {
    required: string,
    minLength: string,
    maxLength: string,
    pattern: string
  }
}

export interface SignUpError {
  name: {
    required: string,
    minLength: string
  },
  email: {
    required: string,
    minLength: string,
    pattern: string
  },
  password: {
    required: string,
    minLength: string,
    maxLength: string,
    pattern: string
  }
}

export interface ProductError {
  name: {
    required: string,
    minLength: string,
  },
  amount: {
    maxLength: string
  }
}

export interface ProductListError {
  name: {
    required: string,
    minLength: string,
  },
  purchase_date: {
    required: string,
  }
}

/**
 * Main set of error messages
 * */
export interface ErrorMessages {
  signin: SignInError,
  signup: SignUpError,
  product: ProductError,
  list: ProductListError
}

@Injectable()
export class FormsService {

  constructor() {
  }

  static checkFormValidity(formType: string,
                           errorMessages: any,
                           formControls: {[control: string]: AbstractControl},
                           force: boolean = false) {


    for (let controlName in errorMessages) if (errorMessages.hasOwnProperty(controlName)) {

      /**
       * Reset previous errors
       * */
      errorMessages[controlName] = [];

      /**
       * Check if we already have error and this control's status is dirty
       * */
      if (formControls[controlName].errors && (formControls[controlName].dirty || force)) {

        /**
         * Loop through all present errors
         * */
        for (let error in formControls[controlName].errors) {

          /**
           * Check if we have prepared messageSet for this error in ERROR_MESSAGES set
           * */
          if (FormsService.ERROR_MESSAGES[formType][controlName][error]) {

            /**
             * Add this error to the errors set
             * */
            errorMessages[controlName].push(FormsService.ERROR_MESSAGES[formType][controlName][error]);

          }
        }
      }

    }
  }

  static ERROR_MESSAGES: ErrorMessages = {
    signin: {
      email: {
        required: 'email is required.',
        minLength: 'email must be at least 4 characters long.',
        pattern: `email is not valid`
      },
      password: {
        required: 'password is required.',
        minLength: 'password must be at least 4 characters long.',
        maxLength: 'password must be not longer than 8 characters.',
        pattern: `password at least has to contain  one lowercase's char, one uppercase's and one number`
      }
    },
    signup: {
      name: {
        required: 'name is required',
        minLength: 'name must be at least 4 characters long.'
      },
      email: {
        required: 'email is required.',
        minLength: 'email must be at least 4 characters long.',
        pattern: `email is not valid`
      },
      password: {
        required: 'password is required.',
        minLength: 'password must be at least 4 characters long.',
        maxLength: 'password must be not longer than 8 characters.',
        pattern: `password at least has to contain  one lowercase's char, one uppercase's and one number`
      }
    },
    product: {
      name: {
        required: 'name is required.',
        minLength: 'name must be at least 2 characters long.',
      },
      amount: {
        maxLength: `amount can't be longer than 10 chars`
      }
    },
    list: {
      name: {
        required: 'name is required.',
        minLength: 'name must be at least 2 characters long.',
      },
      purchase_date: {
        required: 'date of execution is required.'
      },
    }
  }

}
