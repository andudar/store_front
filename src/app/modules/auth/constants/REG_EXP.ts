/**
 * Created by andrey on 11.11.16.
 */
export const REG_EXP_EMAIL: string = '(.+)@(.+){1,}\.(.+){1,}';
export const REG_EXP_PASSWORD: string = '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}';

