import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {FormsService} from './../../services';
import {AuthService} from './../../services';
import {REG_EXP_EMAIL, REG_EXP_PASSWORD} from './../../constants'
import { Router } from "@angular/router";

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html'
})
export class SignInComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {

  }

  public form: FormGroup;

  /* We'll store any errors here */
  public errorMessages: {} = {
    email: [],
    password: []
  };

  public isError: boolean = false;
  public apiError: string;

  ngOnInit() {

    /* Build forms for sign in */
    this.form = this.formBuilder.group({
      'email': new FormControl('',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.pattern(REG_EXP_EMAIL)
        ]
      ),
      'password': new FormControl('',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(8),
          Validators.pattern(REG_EXP_PASSWORD)
        ]
      )
    });

    /* Subscribe to the form changes */
    this.form.valueChanges
      .subscribe(values => {

        /* Reset error */
        this.isError = false;
        this.apiError = null;
        FormsService.checkFormValidity('signin', this.errorMessages, this.form.controls);
      });
  }

  signin(): void {

    /* Check form anyway to prevent broken api calls */
    if (this.form.valid) {
      this.authService.signin(this.form.value).subscribe(res => {
        this.authService.saveUser(res.token);
          this.router.navigate(['/main'])
        },
      error => {

        /* Handle sign in error - show error messages and disable button */
        this.isError = true;
        this.apiError = error.payload.message;
        console.log(error);
      });
    } else {
      FormsService.checkFormValidity('signin', this.errorMessages, this.form.controls, true);
      this.isError = true;
    }
  }

}
