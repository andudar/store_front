import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {FormsService, AuthService} from "../../services";
import {REG_EXP_EMAIL, REG_EXP_PASSWORD} from './../../constants'
import { Router } from "@angular/router";

@Component({
  selector: 'sign-up',
  templateUrl: 'sign-up.component.html'
})
export class SignUpComponent implements OnInit {


  public form: FormGroup;
  public errorMessages: any = {
    name: [],
    email: [],
    password: []
  };
  public isError: boolean = false;
  public apiError: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'name': new FormControl('', [
          Validators.required,
          Validators.minLength(4)
        ]
      ),
      'email': new FormControl('', [
          Validators.required,
          Validators.minLength(4),
          Validators.pattern(REG_EXP_EMAIL)
        ]
      ),
      'password': new FormControl('', [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(8),
          Validators.pattern(REG_EXP_PASSWORD)
        ]
      )
    });

    this.form.valueChanges
      .subscribe(() => {

        /* Reset errors */
        this.isError = false;
        this.apiError = null;
        FormsService.checkFormValidity('signup', this.errorMessages, this.form.controls);
      });
  }

  signUp(): void {

    /* Check form anyway to prevent broken api calls */
    if (this.form.valid) {
      this.authService.signup(this.form.value).subscribe(res => {
        console.log(res)
          this.authService.saveUser(res.token);
        this.router.navigate(['main'])

        },
        error => {

          /* Handle sign up error - show error messages and disable button */
          this.isError = true;
          this.apiError = error.payload.message;
          console.log(error);
        });
    } else {
      FormsService.checkFormValidity('signup', this.errorMessages, this.form.controls, true);
      this.isError = true;
    }
  }

}
