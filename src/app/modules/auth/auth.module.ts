/**
 * Created by andrey on 07.11.16.
 */
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import { RouterModule } from "@angular/router";
import { SignInComponent, SignUpComponent, IndexComponent } from "./components";
import { FormsService, AuthService, LocalStorageService } from "./services";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FormsService, AuthService, LocalStorageService],
  declarations: [SignInComponent, SignUpComponent, IndexComponent]
})

export class AuthModule {}
