import {Injectable} from '@angular/core';
import './../../../rxjs-operators';
import {ApiService} from './../../api/services';
import {AuthService} from './../../auth/services';
import {BehaviorSubject, Observable} from "rxjs";
import {Product} from "./../../../models";

@Injectable()
export class ProductsService {

  private root = '/products';

  constructor(private apiService: ApiService, private authService: AuthService) { }

  /* product handlers */

  private _products: BehaviorSubject<any> = new BehaviorSubject([]);

  public products: Observable<any> = this._products.asObservable();

  public productsChanges: BehaviorSubject<any> = new BehaviorSubject([]);

  public init(listId: number) {
    return this.getAllProducts(listId);
  }

  public reset() {
    this._products.next([]);
  }

  public getAllProducts(listId: number) {
    return this.apiService.call({
      method: 'get',
      url: `${this.root}/lists/${listId}/products`,
      token: this.authService.token
    })
      .subscribe(
        res => {
          this._products.next(res);
        },
        err => console.log("Error retrieving products")
      );
  }

  public addProduct(listId: number, product: Product) {
    let
      obs = this.apiService.call({
        method: 'put',
        url: `${this.root}/lists/${listId}/add-product`,
        token: this.authService.token,
        body: product
      }).share();

    obs.subscribe((res: Product) => {
      return this._products.subscribe(products => {
        console.log('pushed new item to list');

        products.push(res);
        this.productsChanges.next(products);

      }).unsubscribe();//TODO Is it ok???
    });
    return obs;
  }

  public getProduct(productId: number) {
    return this.apiService.call({
      method: 'get',
      url: `${this.root}/lists/products/${productId}`,
      token: this.authService.token
    })
  }

  public updateProduct(productId: number, product: Product) {
    let
      obs: Observable<any> = this.apiService.call({
      method: 'post',
      url: `${this.root}/lists/products/${productId}/update`,
      token: this.authService.token,
      body: {
        name: product.name,
        amount: product.amount
      }
    }).share();

    obs.subscribe((res: Product) => {
        return this._products.subscribe(products => {
          let
            index = products.findIndex(prod => prod._id === productId);

          products[index] = res;
          this.productsChanges.next(products);
        }).unsubscribe();
      },
      err => {
        console.log(err);
      });

    return obs;
  }

  public deleteProduct(listId: number, productId: number) {
    let
      obs: Observable<Product> = this.apiService.call({
        method: 'delete',
        url: `${this.root}/lists/${listId}/products/${productId}/delete`,
        token: this.authService.token
      }).share();

    obs.subscribe(res => {
        return this._products.subscribe(products => {
          let
            index = products.findIndex(product => product._id === productId);
          products.splice(index, 1);
          this.productsChanges.next(products);
        }).unsubscribe();
      },
      err => {
        console.log(err);
      });

    return obs;
  }

  public toggleProductStatus(productId: number, status) {
    let
      obs: Observable<Product[]> = this.apiService.call({
        method: 'post',
        url: `${this.root}/lists/products/${productId}/status`,
        token: this.authService.token,
        body: {
          status: status
        }
      }).share();

    obs.subscribe(res => {
        return this._products.subscribe(products => {
          let
            index = products.findIndex(prod => prod._id === productId);

          products[index].status = status;
          // this.productsChanges.next(products); // TODO demystifying this part
        }).unsubscribe();
      },
      err => {
        console.log(err);
      });

    return obs;
  }


}
