import { Injectable } from '@angular/core';
import { ApiService } from "../../api/services";
import { AuthService } from "../../auth/services/auth.service";

const ROOT_PATH = '/profile';

@Injectable()
export class UserService {

  constructor(private apiService: ApiService, private authService: AuthService) {
  }

  public getProfile() {
    return this.apiService.call({
      method: 'get',
      url: `${ROOT_PATH}/user`,
      token: this.authService.token
    })
  }

  public updateProfile(name: string, email: string) {
    return this.apiService.call({
      method: 'post',
      url: `${ROOT_PATH}/user/update`,
      body: {name, email},
      token: this.authService.token
    }).subscribe(({token}: {token: string}) => {
      this.authService.saveUser(token);
    })
  }

  public checkPassword() {
    return this.apiService.call({
      method: 'post',
      url: `${ROOT_PATH}/user/password`,
      token: this.authService.token
    })
  }

  public deleteProfile() {
    return this.apiService.call({
      method: 'delete',
      url: `${ROOT_PATH}/user/delete`,
      token: this.authService.token
    })
  }

}
