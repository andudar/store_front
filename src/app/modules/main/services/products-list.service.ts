import {Injectable} from '@angular/core';
import {ApiService} from './../../api/services';
import {AuthService} from './../../auth/services';
import {BehaviorSubject, Observable} from "rxjs";
import {ProductList} from "../../../models/ProductList";

interface List {
  name: string,
  purchase_date: any
}

@Injectable()
export class ProductsListService {

  private root = '/products';

  private _product_lists: BehaviorSubject<any> = new BehaviorSubject([]);

  public product_lists: Observable<any> = this._product_lists.asObservable();

  constructor(private apiService: ApiService, private authService: AuthService) {
    //this.getAllLists();
  }

  public init() {
    this.getAllLists();
  }

  private getAllLists() {
    this.apiService.call({
      method: 'get',
      url: `${this.root}/lists`,
      token: this.authService.token
    })
      .subscribe(
        res => {
          this._product_lists.next(res);
        },
        err => console.log("Error retrieving product lists")
      );
  }

  /* list handlers */

  public createList(name, purchase_date) {
    console.log(name, purchase_date);
    let
      obs = this.apiService.call({
        method: 'post',
        url: `${this.root}/lists/create`,
        token: this.authService.token,
        body: {
          name: name,
          purchase_date: purchase_date
        }
      }).share();

    obs.subscribe(res => {
      return this._product_lists.subscribe(lists => {
        lists.push(res);
      }).unsubscribe();
    });
    return obs;
  }

  public getList(listId) {
    return this.apiService.call({
      method: 'get',
      url: `${this.root}/lists/${listId}`,
      token: this.authService.token
    })
  }

  public getProductsList(listId) {
    return this.apiService.call({
      method: 'get',
      url: `${this.root}/lists/${listId}/products`,
      token: this.authService.token
    })
  }

  deleteList(listId): Observable<any> {
    let
      obs: Observable<any> = this.apiService.call({
        method: 'delete',
        url: `${this.root}/lists/${listId}/delete`,
        token: this.authService.token
      });

    obs.subscribe(res => {
        return this._product_lists.subscribe(lists => {
          let
            index = lists.findIndex(list => list._id === listId);
          lists.splice(index, 1);
        }).unsubscribe();
      }
    );
    return obs;
  }

  updateList(listId, list: List) {
    console.log(list);
    let
      obs: Observable<any> = this.apiService.call({
        method: 'post',
        url: `${this.root}/lists/${listId}/update`,
        token: this.authService.token,
        body: {
          name: list.name,
          purchase_date: list.purchase_date
        }
      });

    obs.subscribe(
      res => {
        return this._product_lists.subscribe(lists => {
          let
            index = lists.findIndex(list => list._id === listId);

          lists.splice(index, 1);
          lists.push(res);
        }).unsubscribe();
      },
      err => {
        console.log(err);
      }
    );

    return obs;
  }

}
