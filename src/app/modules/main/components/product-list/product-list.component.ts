import {Component, OnInit, Input, ChangeDetectorRef} from '@angular/core';

import {Product, ProductList} from './../../../../models';
import {ProductsService} from "../../services";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {

  private listId;
  private products;
  /* List of items to show on the current page */
  private productsToShow: Product[] = [];
  private itemsOnPage: number = 5;
  /* It's a change detector */
  public cdr;
  public statusSubscription: Subscription;
  public productsChangeSubscription: Subscription;

  constructor(cdr: ChangeDetectorRef,
              private route: ActivatedRoute,
              private router: Router,
              private productService: ProductsService) {
    this.cdr = cdr;

    this.route.params.subscribe(el => {
      /* Reset product list to prevent previous list blinking when we go to another list */
      this.listId = el['id'];
      /* Load all products for this list */
      this.productService.init(this.listId);
    });
  }

  ngOnInit() {
    this.productService.reset();

    this.productService.products.subscribe(products => {
      /* Get products on bootstrap */
      this.products = products;
      //this.productsToShow = this.products.slice(0, this.itemsOnPage);
    });

    this.productsChangeSubscription = this.productService.productsChanges.subscribe(products => {
      /* Update products when the length was changed */
      this.products = [...products];
    })
  }

  public deleteProduct(product: Product) {
    this.productService.deleteProduct(this.listId, product._id).subscribe(deletedProduct => {
      this.products = [...this.products];
    });
  }

  public editProduct(product: Product) {
    this.router.navigate([{
      outlets: {
        popup: [
          this.listId,
          'add-product',
          {
            product: product._id
          }
        ],
      }
    }]);
  }

  public toggleStatus(product) {
    console.log(product);
    let
      status = product.status == 'not done' ? 'done' : 'not done';

      this.statusSubscription = this.productService.toggleProductStatus(product._id, status)
        .subscribe(res => {
            console.log(`status from ${product.status} was changed to ${status}`);
        },
        err => {
          console.error(err);
        });
  }

  //TODO directive for tooltips on hover

  public onItemsToShow(list: Product[]) {
    this.productsToShow = list;
    /* Make change detector running */
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.statusSubscription && this.statusSubscription.unsubscribe();
    this.productsChangeSubscription && this.productsChangeSubscription.unsubscribe();
  }

}
