import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from "@angular/router";
import {ProductsListService} from "../../services";

@Component({
  selector: 'product-list-all',
  templateUrl: './product-list-all.component.html',
  styleUrls: ['./product-list-all.component.scss']
})
export class ProductListAllComponent implements OnInit {

  public productLists;
  public hello;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productListService: ProductsListService) {

    this.productListService.init();

    // route.data.subscribe(data => {
    //   this.productLists = data['productLists'];
    //   console.log(data['hello']);
    // });
    // this.hello = route.snapshot.data['hello'];
  }

  ngOnInit() {
    this.productLists = this.productListService.product_lists;
  }

  public goToList(listId) {
    return this.router.navigate([`main/list/${listId}`])
  }

  public deleteList(listId) {
    this.productListService.deleteList(listId)
  }

  public updateList(listId) {
    this.router.navigate([{outlets: {
      popup: [
        'create-list',
        {
          listId: listId
        }
      ]
    }
    }])
  }

}
