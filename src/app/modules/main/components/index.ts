/**
 * Created by andrey on 10.11.16.
 */
export { MainComponent } from "./main/main.component";
export { NavbarComponent } from "./navbar/navbar.component";
export { ProductListComponent } from './product-list/product-list.component';
export { ProductListAllComponent } from './product-list-all/product-list-all.component';
export { ProfileComponent } from './profile/profile.component';
export { StaticsComponent } from './statics/statics.component';
