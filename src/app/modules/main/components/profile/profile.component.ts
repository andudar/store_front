import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../auth/services/auth.service";
import {User} from "../../../../models";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
  }

  private user: User;
  public form: FormGroup;

  ngOnInit() {
    this.user = this.authService.getUser();
  }


  public goToEdit() {
    this.router.navigate([{
      outlets: {
        popup: ['update-profile'],
      }
    }]);
  }
}
