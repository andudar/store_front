import {Component, OnInit, Input} from '@angular/core';
import {AuthService} from "../../../auth/services";
import {Router} from "@angular/router";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() active;

  public isLogged = this.authService.isLogged;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  public signOut() {
    this.authService.logout();
  }

  public goToSignIn() {
    this.router.navigate(['/signin']);
  }

}
