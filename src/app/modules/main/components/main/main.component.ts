import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsListService } from "../../services";

@Component({
  selector: 'home',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.scss']
})
export class MainComponent implements OnInit {

  public products: any;
  public type: any;
  public errorMessage: string;


  constructor(private productService: ProductsListService) { }

  ngOnInit() {
  }

}
