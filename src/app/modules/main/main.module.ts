import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MainComponent,
  NavbarComponent,
  ProductListComponent,
  ProductListAllComponent,
  ProfileComponent,
  StaticsComponent
} from './components';
import { ApiModule } from '../api/api.module';
import { SharedModule } from './../../shared';
import { RouterModule } from '@angular/router';
import { ProductsListService, ProductsService, UserService } from './services';

import { ProductsResolver, UserResolver } from './../../routing/resolvers';
import { ReactiveFormsModule } from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    ApiModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    ProductListComponent,
    ProductListAllComponent,
    ProfileComponent,
    StaticsComponent
  ],
  providers: [
    ProductsListService,
    ProductsService,
    UserService,
    ProductsResolver,
    UserResolver
  ]
})
export class MainModule { }
