import {Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import './../../../rxjs-operators';
import {environment} from "../../../../environments/environment";

interface httpParams {
  method: string,
  url: string,
  token?: string,
  body?: any
}

@Injectable()
export class ApiService {

  constructor(private http: Http) {
  }

  public call(params: httpParams): Observable<any> {
    let
      httpParams: Array<any> = [environment.api.endpoint + params.url],
      headers = new Headers({
        'Content-Type': 'application/json'
      }),
      options: RequestOptions;

    if (params.token) {
      headers.append('Authorization', 'JWT ' + params.token);
    }

    if (params.body) {
      httpParams.push(params.body);
    }

    options = new RequestOptions({headers, withCredentials: true});
    httpParams.push(options);

    return this.http[params.method](...httpParams)
      .map(this.extractData)
       //TODO is cache() needed here?
      .catch(this.handleError)
  }

  private extractData(res) {
    let
      body;

    try {
      body = res.json();
    } catch (err) {
      if (typeof res._body === 'string') {
        return res._body;
      } else {
        console.log(err);
      }
    }
    return body || {};
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let
      errMsg: {
        message: string,
        payload: any
      } = {
        message: '',
        payload: {}
      };

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      console.log(err);

      try {
        errMsg.payload = JSON.parse(err);
      } catch (err) {
        errMsg.payload = {};
        console.log(`Can't parse payload`);
      }

      errMsg.message = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg.message = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
