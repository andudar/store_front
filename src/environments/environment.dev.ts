/**
 * Created by andrey on 10.11.16.
 */
export const environment = {
  production: false,
  api: {
    front: 'http://localhost:4200',
    endpoint: 'http://localhost:3003'
  }
};
